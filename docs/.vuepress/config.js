module.exports = {
    title: 'GitLab & CodeFreezR ❤️ VuePress',
    description: 'Vue-powered static site generator running on GitLab Pages',
    base: '/vuepresso/',
    dest: 'public'
}